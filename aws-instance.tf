resource "aws_instance" "jenkins" {
  ami           = var.aws-instance-ami # RedHat - replace this if needed
  instance_type = var.aws-instance-type

  key_name               = var.aws-instance-key
  vpc_security_group_ids = [aws_security_group.jenkins_sg.id]
  subnet_id              = aws_subnet.jenkins_subnet.id

  user_data = <<-EOF
  #!/bin/bash
  cd /tmp
  sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
  sudo systemctl enable amazon-ssm-agent
  sudo systemctl start amazon-ssm-agent
  sudo dnf install wget -y
  sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat/jenkins.repo
  sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io-2023.key
  sudo dnf upgrade
  # Add required dependencies for the jenkins package
  sudo dnf install java-11-openjdk -y
  sudo dnf install jenkins -y
  sudo systemctl start jenkins
  sudo systemctl enable jenkins
  sudo systemctl status jenkins
  EOF

  tags = {
    Name = "jenkins"
  }
}
