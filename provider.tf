terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.63"
    }
  }
}

provider "aws" {
  region = "us-west-2" # Replace with your preferred AWS region
  # credentials for authentication can be set in a number of ways 
  # it's recommended not to hardcode them in the configuration file for security
}

